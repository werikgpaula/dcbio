var wordLimit = 50;


function loadEventos() {
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "http://localhost:8080/eventos",
        data: "",
        success: function (result) {
            var eventos = result;
            var listaEventos = document.getElementById('eventos');
            listaEventos = "";
            for (var aux = 0; aux < eventos.length; aux++) {
                listaEventos = listaEventos + '<div class="fea">';
                listaEventos += '<div class="col-md-4">';
                listaEventos += '<div class="heading pull-right">';
                listaEventos += '<h4><a onclick="saveCodeEvent(' + eventos[aux].codigoEvento + ')" href="' + (eventos[aux].codigoEvento) + '"><span>' + (eventos[aux].nomeEvento) + '</span></a></h4>';
                listaEventos += '<p><span class="descEventoLer">' + (eventos[aux].descricaoEvento) + '</span></p>';
                listaEventos += '<p><span">' + (eventos[aux].localEvento) + '</span></p>';
                listaEventos += '<p><span">' + (eventos[aux].horarioEvento) + '</span></p>';
                listaEventos += '</div></div></div>';
                listaEventos += '</li>';
            }

            document.getElementById('eventos').innerHTML = listaEventos;
        },
        error: function (result, status, er) {
            console.log(result)
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar eventos: loadEventos()");
        }
    });
}



function carregarListaHortalicas(){
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "http://localhost:8080/carregarhortalicas", 
		data: "",
		success: function (result){
			var hortalicas = result;
			var listaHortalicas = $('#listaHortalicas');
				listaHortalicas = "";
				for(i = 0; i< hortalicas.length; i++){
					if(i == hortalicas.length-1)
						listaHortalicas += '<div class = "col-md-12"><a onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/hortalicas/' + (hortalicas[i].codigo) + '"><label class="labelHortalica" style="border-bottom: 1px solid;">' + (hortalicas[i].nome) + '</label></a></div>';
					else
						listaHortalicas += '<div class = "col-md-12"><a onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/hortalicas/' + (hortalicas[i].codigo) + '"><label class="labelHortalica">' + (hortalicas[i].nome) + '</label></a></div>';
				}
			
			$('#listaHortalicas').append(listaHortalicas);
		},
		error: function(result, status, er){
            console.log(result)
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar hortalicças: carregarListaHortalicas()");
        }
	});
}

function carregarDetalhesHortalica(){
	var codigoHortalica = sessionStorage.getItem("codigoHortalica");
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "http://localhost:8080/hortalicas/detalhesHortalica?codigoHortalica=" + codigoHortalica,
		data: codigoHortalica,
		
		success: function (result){
			var hortalica = result;
			var detalhesHortalica = document.getElementById('hortalicaDetail');
			var detalhes = detalhesHortalica.innerHTML;
			detalhesHortalica = "";
			detalhesHortalica = detalhes.replace("NOMEHORTALICA", hortalica.nome)
				.replace("EPOCAPLANTIO", hortalica.melhorEpocaPlantio)
				.replace("PERIODOGERMINACAO", hortalica.periodoGerminacao)
				.replace("TEMPOENTRESAFRAS", hortalica.tempoEntreSafras)
				.replace("ESPACAMENTOCOVAS", hortalica.espacamentoCovas)
				.replace("PROFUNDIDADESEMENTE", hortalica.profundidadeSemente)
				.replace("IRRIGACAODIA", hortalica.irrigacaoPorDia)
				.replace("PRODUTIVIDADEESPERADA", hortalica.produtividadeEsperada)
				.replace("TIPOPLANTIO", hortalica.tipoPlantio)
				.replace("OBSERVACOES", hortalica.observacoes)
				.replace("../img/beneficioAlface.jpg", hortalica.imagem);
			document.getElementById('hortalicaDetail').innerHTML = detalhesHortalica;
		},
		error: function(result, status, er){
            console.log(result);
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar detalhes da hortaliça: carregarDetalhesHortalica()");
        }
	});
}

function carregarListaHortalicasEdicao(){
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "http://localhost:8080/carregarhortalicas",
		data: "",
		success: function (result){
			var hortalicas = result;
			var listaHortalicas = $('#listaHortalicasEdicao');
				listaHortalicas = "";
				for(i = 0; i< hortalicas.length; i++){
					if(i == hortalicas.length-1)
						listaHortalicas += '<div class = "col-md-12"><label class="labelHortalica" style="border-bottom: 1px solid;"><a onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/hortalicas/' + (hortalicas[i].codigo) + '">' + (hortalicas[i].nome) + 
						'</a><a class="button" onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/cadastrarItemDeTroca/deletar/' + (hortalicas[i].codigo) + '">Deletar</a></label></div>';
					else
						listaHortalicas += '<div class = "col-md-12"><label class="labelHortalica"><a onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/hortalicas/' + (hortalicas[i].codigo) + '">' + (hortalicas[i].nome) + 
						'</a><a class="button" onclick="salvarCodigoHortalica(' + hortalicas[i].codigo + ')" href="/cadastrarItemDeTroca/deletar/' + (hortalicas[i].codigo) + '">Deletar</a></label></div>';
				}
			
			$('#listaHortalicasEdicao').append(listaHortalicas);
		},
		error: function(result, status, er){
            console.log(result);
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar hortaliças: carregarListaHortalicasEdicao()");
        }
	});
}

function carregarListaItensDeTrocaEdicao(){
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "http://localhost:8080/carregarItensDeTroca",
		data: "",
		success: function (result){
			var itensDeTroca = result;
			var listaItensDeTroca = $('#listaItensDeTroca');
				listaItensDeTroca = "";
				for(i = 0; i< itensDeTroca.length; i++){
					if(i == itensDeTroca.length-1)
						listaItensDeTroca += '<div class = "col-md-12"><label class="labelItemDeTroca" >' + (itensDeTroca[i].nomeItemDeTroca) + " - " +
						'</a><a class="button" onclick="salvarCodigoItemDeTroca(' + itensDeTroca[i].codigoItemDeTroca + ')" href="/cadastrarItemDeTroca/deletar/' + (itensDeTroca[i].codigoItemDeTroca) + '"> Deletar </a></label></div>';
					else
						listaItensDeTroca += '<div class = "col-md-12"><label class="labelItensDeTroca">' + (itensDeTroca[i].nomeItemDeTroca) + " - " +
						'</a><a class="button" onclick="salvarCodigoItemDeTroca(' + itensDeTroca[i].codigoItemDeTroca + ')" href="/cadastrarItemDeTroca/deletar/' + (itensDeTroca[i].codigoItemDeTroca) + '">Deletar</a></label></div>';
				}

			$('#listaItensDeTroca').append(listaItensDeTroca);
            console.log(listaItensDeTroca);
		},
		error: function(result, status, er){
            console.log(result);
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar itens de troca: carregarListaItensDeTrocaEdicao()");
        }
	});
}

function carregarListaItensDeTroca(){
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
		url: "http://localhost:8080/carregarItensDeTroca",
		data: "",
		success: function (result){
			var itensDeTroca = result;
			var listaItensDeTroca = $('#listaItensDeTroca');
				listaItensDeTroca = "";


				for(i = 0; i< itensDeTroca.length; i++){
						listaItensDeTroca += '<input type="checkbox"  name="itensDeTroca" value="'+itensDeTroca[i].codigoItemDeTroca+'">'+itensDeTroca[i].nomeItemDeTroca+'<br>';


					}

			$('#listaItensDeTroca').append(listaItensDeTroca);

		},
		error: function(result, status, er){
            console.log(result);
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar itens de troca: carregarListaItensDeTroca()");
        }
	});
}


function deletarHortalica(){
	var codigoHortalica = sessionStorage.getItem("codigoHortalica");
	$.ajax({
		type: "GET",
		contentType: "application/json; charset=utf-8",
		dataType: "json",
        url: "http://localhost:8080/cadastrarItemDeTroca/deletar?codigoHortalica=" + codigoHortalica,
        data: codigoHortalica,
        
        sucess: function(){
        	alert("Hortalica deletada com sucesso!");
        },
        error: function(status, er){
        	console.log(status);
            console.log(er);
            console.log("Erro na rotina de deletar uma hortaliça: deletarHortalica()");
        }
	});
}

function salvarCodigoHortalica(valueCode){
	sessionStorage.setItem("codigoHortalica", valueCode);
}


function salvarCodigoItemDeTroca(valueCode){
	sessionStorage.setItem("codigoItemDeTroca", valueCode);
}

function loadDetalhesEvento() {
    var codigoEvento = sessionStorage.getItem("codigoEvento");
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "http://localhost:8080/eventoDetail?codigoEvento=" + codigoEvento,
        data: codigoEvento,
        success: function (result) {
            var evento = result;
            var eventoDetail = document.getElementById('eventoDetail');
            eventoDetail = "";
            eventoDetail = eventoDetail + '<div class="header-section text-center">';
            eventoDetail += '<h2>' + (evento.nomeEvento) + '</h2>';
            eventoDetail += '<p>' + (evento.descricaoEvento) + '</p></div>';
            eventoDetail += '<div class="feature-info"><div class="fea"><div class="heading">';
            eventoDetail += '<p><span">Local do Evento: ' + (evento.localEvento) + '<br>Data: ' + (evento.dataEvento) + '<br>Horário: ' + (evento.horarioEvento) + '</span></p>';

            if(evento.itensDeTroca!= ""){
            eventoDeTroca(evento)
            }
            document.getElementById('eventoDetail').innerHTML = eventoDetail;

        },
        error: function (result, status, er) {
            console.log(result)
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar detalhe evento: loadDetalhesEvento()");
        }
    });
}

function eventoDeTroca(evento){

            var evento = evento.itensDeTroca.split(',');
            for(i=0;i<evento.length;i++){
            var eventoDetail = document.getElementById('eventoDeTroca');
            eventoDeTroca = "";

            eventoDeTroca += '<label class="col-md-12 control-label">Selecione uma Doação (Obrigatório Para Participar)</label>';


             $.ajax({


                    type: "GET",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    url: "http://localhost:8080/carregarItemDeTroca/"+evento[i],
                    data: evento,
                    success: function (result) {
                        var itemDeTroca = result;
                        eventoDeTroca += '<input type="checkbox"  name="doacaoParticipante" value="'+itemDeTroca.codigoItemDeTroca+'">'+itemDeTroca.nomeItemDeTroca+'<br>';
            document.getElementById('eventoDeTroca').innerHTML = eventoDeTroca;

}});}

}

function saveCodeEvent(valueCode) {
    sessionStorage.setItem("codigoEvento", valueCode);
}

// Função para executar a validação de login ao entrar em uma pagina de administrador
function realizaLogin() {
    var emailAdm = document.getElementById('emailAdm').value;
    var senhaAdm = document.getElementById('senhaAdm').value;  
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "http://localhost:8080/admLogin?emailAdm="+emailAdm,
        data: emailAdm,
        success: function (result) {
            if(result.nomeAdm == undefined){
                alert("Usuario não encontrado.")
            }else if(senhaAdm != result.senhaAdm){
                alert("Senha Incorreta.");
            }else{
                sessionStorage.setItem('emailAdm', emailAdm);
                sessionStorage.setItem('senhaAdm', senhaAdm);
                alert("Login efetuado com sucesso! \nVocê será redirecionado à Página de Administrador.");
                window.location.replace("/cadastrarHortalica");
            }
        },
        error: function (data, status, er) {
            console.log(data);
            console.log(er);
            console.log(status);
            alert("Usuario não encontrado.");
        }

    });

}

function validaLogin() {
    var emailAdm = sessionStorage.getItem("emailAdm");
    var senhaAdm = sessionStorage.getItem("senhaAdm");

    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "http://localhost:8080/admLogin?emailAdm="+emailAdm,
        data: emailAdm,
        success: function (result) {
            if(result.nomeAdm == undefined){
                alert("Usuario não encontrado.")
                window.location.replace("/login");
            }else if(senhaAdm != result.senhaAdm){
                alert("Senha Incorreta.");
                window.location.replace("/login");
            }else if(emailAdm != result.emailAdm){
                alert("Necessário realizar login.");
                window.location.replace("/login");
            }
        },
        error: function (data, status, er) {
            console.log(data);
            console.log(er);
            console.log(status);
            alert("Necessário realizar login.");
            window.location.replace("/login");
        }

    });
}

function limpaLogin(){
    sessionStorage.removeItem('emailAdm');
    sessionStorage.removeItem('senhaAdm');
}

function carregarItensDeTroca(){
    $.ajax({
        type: "GET",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        url: "http://localhost:8080/carregarItensDeTroca",
        data: "",
        success: function (result){
            var itensDeTroca = result;
            
            $('#listaItensDeTroca').append(listaItensDeTroca);
            console.log(listaItensDeTroca);
        },
        error: function(result, status, er){
            console.log(result);
            console.log(status);
            console.log(er);
            console.log("Erro na rotina de carregar itens de troca: carregarListaItensDeTrocaEdicao()");
        }
    });
}

