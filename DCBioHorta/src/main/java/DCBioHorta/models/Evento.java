package DCBioHorta.models;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
public class Evento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long codigoEvento;

	@NotBlank (message = "Nome é uma informação obrigatória.")
	private String nomeEvento;
	@NotBlank (message = "Local do Evento é uma informação obrigatória.")
	private String localEvento;
	@NotBlank (message = "Data do Evento é uma informação obrigatória.")
	private String dataEvento;
	@NotEmpty (message = "Horário do Evento é uma informação obrigatória.")
	@NotBlank String horarioEvento;
	@Column(length = 99999)
	@NotBlank (message = "Descrição é uma informação obrigatória.")
	private String descricaoEvento;
	@Column (name="itens_de_troca")
	private String itensDeTroca ="";
	
	@OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
	@JoinTable(name= "evento_participante", joinColumns = @JoinColumn(name= "evento_codigo_evento", referencedColumnName = "codigoEvento"))
	private List<Participante> participante;


	public String getItensDeTroca() {
		return itensDeTroca;
	}

	public void setItensDeTroca(String itensDeTroca) {
		this.itensDeTroca = itensDeTroca;
	}
	
	public long getCodigoEvento() {
		return codigoEvento;
	}

	public void setCodigoEvento(long codigoEvento) {
		this.codigoEvento = codigoEvento;
	}

	public String getNomeEvento() {
		return nomeEvento;
	}

	public void setNomeEvento(String nomeEvento) {
		this.nomeEvento = nomeEvento;
	}

	public String getLocalEvento() {
		return localEvento;
	}

	public void setLocalEvento(String localEvento) {
		this.localEvento = localEvento;
	}

	public String getDataEvento() {
		return dataEvento;
	}

	public void setDataEvento(String dataEvento) {
		this.dataEvento = dataEvento;
	}

	public String getHorarioEvento() {
		return horarioEvento;
	}

	public void setHorarioEvento(String horarioEvento) {
		this.horarioEvento = horarioEvento;
	}

	public String getDescricaoEvento() {
		return descricaoEvento;
	}

	public void setDescricaoEvento(String descricaoEvento) {
		this.descricaoEvento = descricaoEvento;
	}

}
