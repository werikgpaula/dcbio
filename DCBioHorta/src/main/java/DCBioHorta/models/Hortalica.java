package DCBioHorta.models;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class Hortalica implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long codigo;
	
	@NotBlank(message = "Nome é uma informação obrigatória.")
	private String nome;
	private String descricao;	
	private String melhorEpocaPlantio;
	@NotBlank(message = "Período Germinação é uma informação obrigatória.")
	private String periodoGerminacao;
	@NotBlank(message = "Tempo Entre Safras é uma informação obrigatória.")
	private String tempoEntreSafras;
	@NotBlank(message = "Espaçamento Das Covas é uma informação obrigatória.")
	private String espacamentoCovas;
	private String profundidadeSemente;
	@NotNull(message="Irrigação por Dia é uma informação obrigatória.")
	private int irrigacaoPorDia;
	private String produtividadeEsperada;
	@NotBlank(message = "Tipo de Plantio é uma informação obrigatória.")
	private String tipoPlantio;
	private String observacoes;
	private String imagem;
	
	//Getters e Setters
	public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getMelhorEpocaPlantio() {
		return melhorEpocaPlantio;
	}
	public void setMelhorEpocaPlantio(String melhorEpocaPlantio) {
		this.melhorEpocaPlantio = melhorEpocaPlantio;
	}
	public String getPeriodoGerminacao() {
		return periodoGerminacao;
	}
	public void setPeriodoGerminacao(String periodoGerminacao) {
		this.periodoGerminacao = periodoGerminacao;
	}
	public String getTempoEntreSafras() {
		return tempoEntreSafras;
	}
	public void setTempoEntreSafras(String tempoEntreSafras) {
		this.tempoEntreSafras = tempoEntreSafras;
	}
	public String getEspacamentoCovas() {
		return espacamentoCovas;
	}
	public void setEspacamentoCovas(String espacamentoCovas) {
		this.espacamentoCovas = espacamentoCovas;
	}
	public String getProfundidadeSemente() {
		return profundidadeSemente;
	}
	public void setProfundidadeSemente(String profundidadeSemente) {
		this.profundidadeSemente = profundidadeSemente;
	}
	public int getIrrigacaoPorDia() {
		return irrigacaoPorDia;
	}
	public void setIrrigacaoPorDia(int irrigacaoPorDia) {
		this.irrigacaoPorDia = irrigacaoPorDia;
	}
	public String getProdutividadeEsperada() {
		return produtividadeEsperada;
	}
	public void setProdutividadeEsperada(String produtividadeEsperada) {
		this.produtividadeEsperada = produtividadeEsperada;
	}
	public String getTipoPlantio() {
		return tipoPlantio;
	}
	public void setTipoPlantio(String tipoPlantio) {
		this.tipoPlantio = tipoPlantio;
	}
	public String getObservacoes() {
		return observacoes;
	}
	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public void setIrrigacaoPorDia(Integer irrigacaoPorDia) {
		this.irrigacaoPorDia = irrigacaoPorDia;
	}
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	
}
