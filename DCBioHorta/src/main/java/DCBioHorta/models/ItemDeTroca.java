package DCBioHorta.models;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class ItemDeTroca implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long codigoItemDeTroca;

    @NotBlank(message = "Nome do Item de Troca é uma informação obrigatória")
    private String nomeItemDeTroca;


    public String getNomeItemDeTroca() {
        return nomeItemDeTroca;
    }

    public void setNomeItemDeTroca(String nomeItemDeTroca) {
        this.nomeItemDeTroca = nomeItemDeTroca;
    }


    public long getCodigoItemDeTroca() {
        return codigoItemDeTroca;
    }

    public void setCodigoItemDeTroca(long codigoItemDeTroca) {
        this.codigoItemDeTroca = codigoItemDeTroca;
    }

}
