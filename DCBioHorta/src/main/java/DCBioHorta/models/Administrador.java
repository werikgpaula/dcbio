package DCBioHorta.models;

import org.hibernate.annotations.Cascade;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


@Entity
public class Administrador implements Serializable{
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    long codigoAdm;

    @NotEmpty
    private String nomeAdm;
    @NotEmpty
    private String emailAdm;
    @NotEmpty
    private String senhaAdm;

    public long getCodigoAdm() {
        return codigoAdm;
    }

    public void setCodigoAdm(long codigoAdm) {
        this.codigoAdm = codigoAdm;
    }

    public String getNomeAdm() {
        return nomeAdm;
    }

    public void setNomeAdm(String nomeAdm) {
        this.nomeAdm = nomeAdm;
    }

    public String getEmailAdm() {
        return emailAdm;
    }

    public void setEmailAdm(String emailAdm) {
        this.emailAdm = emailAdm;
    }

    public String getSenhaAdm() {
        return senhaAdm;
    }

    public void setSenhaAdm(String senhaAdm) {
        this.senhaAdm = senhaAdm;
    }




}
