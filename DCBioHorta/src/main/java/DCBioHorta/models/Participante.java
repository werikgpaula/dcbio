package DCBioHorta.models;

import org.hibernate.annotations.Cascade;

import java.io.Serializable;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;


@Entity
public class Participante implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	long codigoParticipante;

	@NotEmpty
	private String nomeParticipante;
	@NotEmpty
	private String emailParticipante;
	@NotEmpty
	private String doacaoParticipante;


	@ManyToOne
	@JoinTable(name= "evento_participante", joinColumns = @JoinColumn(name= "participante_codigo_participante", referencedColumnName = "codigoParticipante"))
	private Evento evento;

	public String getNomeParticipante() {
		return nomeParticipante;
	}

	public void setNomeParticipante(String nomeParticipante) {
		this.nomeParticipante = nomeParticipante;
	}

	public Evento getEvento() {
		return evento;
	}

	public void setEvento(Evento evento) {
		this.evento = evento;
	}

	public long getCodigoParticipante() {
		return codigoParticipante;
	}

	public void setCodigoParticipante(long codigoParticipante) {
		this.codigoParticipante = codigoParticipante;
	}

	public String getDoacaoParticipante() {
		return doacaoParticipante;
	}

	public void setDoacaoParticipante(String doacaoParticipante) {
		this.doacaoParticipante = doacaoParticipante;
	}


	public String getEmailParticipante() {
		return emailParticipante;
	}

	public void setEmailParticipante(String emailParticipante) {
		this.emailParticipante = emailParticipante;
	}

}
