package DCBioHorta.commons;

public enum tipoTroca {

	LIVRO(01, "Livro"),SEMENTE(02, "Semente"),AGASALHO(03, "Agasalho");
	private final String descricao;
	private final Integer codigo;
	
	private tipoTroca(Integer codigo, String descricao) {
		this.descricao = descricao;
		this.codigo = codigo;
	}
	public String getDescricao() {
		return descricao;
	}
	public Integer getCodigo() {
		return codigo;
	}
		
}
