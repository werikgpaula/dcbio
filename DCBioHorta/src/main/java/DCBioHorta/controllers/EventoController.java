package DCBioHorta.controllers;

import javax.validation.Valid;
import java.util.List;

import DCBioHorta.models.ItemDeTroca;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.*;

import DCBioHorta.models.Evento;
import DCBioHorta.models.Participante;
import DCBioHorta.repository.EventoRepository;
import DCBioHorta.repository.ParticipanteRepository;

@RestController
public class EventoController {
    @Autowired
    private EventoRepository er;

    @Autowired
    private ParticipanteRepository pr;


    @RequestMapping(value = "/cadastrarEvento", method = RequestMethod.POST)
    public RedirectView form(@Valid Evento evento, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            attributes.addFlashAttribute("mensagem", result.getFieldError().getDefaultMessage());
            return new RedirectView("/cadastrarEvento");
        }
        er.save(evento);
        attributes.addFlashAttribute("mensagem", "Cadastro realizado com sucesso!");
        return new RedirectView("/cadastrarEvento");
    }


    //Método para listar os eventos que estão na tabela
    @RequestMapping(value = "/eventos")
    public List<Evento> getEventos() {
        List<Evento> eventos = er.findAllByOrderByDataEventoAsc();
        return eventos;
    }


    @RequestMapping(value = "/eventoDetail")
    public Evento findByCodigoEvento(long codigoEvento) {
        return er.findByCodigoEvento(codigoEvento);
    }

    @RequestMapping("/deletarEvento")
    public String deletarEvento(long codigoEvento) {
        Evento evento = er.findByCodigoEvento(codigoEvento);
        er.delete(evento);
        return "redirect:/cadastrarEvento";
    }


    @RequestMapping("/deletarParticipante")
    public String deletarParticipante(long codigoParticipante) {
        Participante participante = pr.findByCodigoParticipante(codigoParticipante);
        pr.delete(participante);
        return "redirect:/detalhesEvento";
    }
}
