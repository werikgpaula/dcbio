package DCBioHorta.controllers;

import DCBioHorta.models.Evento;
import DCBioHorta.models.Hortalica;
import DCBioHorta.models.Participante;
import DCBioHorta.repository.EventoRepository;
import DCBioHorta.repository.HortalicaRepository;
import DCBioHorta.repository.ParticipanteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class NavegacaoPagesController {
    @Autowired
    private EventoRepository er;

    @Autowired
    private ParticipanteRepository pr;
    
    @Autowired
    private HortalicaRepository hr;

    //Método que irá carregar os detalhes do evento na view
    @RequestMapping(value="/{codigoEvento}", method=RequestMethod.GET)
    public ModelAndView detalhesEvento(@PathVariable("codigoEvento") long codigoEvento) {
        Evento evento = er.findByCodigoEvento(codigoEvento);
        ModelAndView mv = new ModelAndView("/evento/detalhesEvento");
        mv.addObject("evento", evento);

        //Irá carregar os participantes relacionados ao evento na aba de detalhes do mesmo
//		Iterable<Participante> participantes = pr.findByEvento(evento);
//		mv.addObject("participantes", participantes);

        return mv;
    }

    @RequestMapping("/cadastrarEvento")
    public ModelAndView eventosCadastrados() {
        ModelAndView mv = new ModelAndView("/evento/formEvento");
        Iterable<Evento> eventos = er.findAll();
        mv.addObject("eventos", eventos);
        return mv;
    }
    
    //Método que irá carregar os detalhes da hortalica na view
    @RequestMapping(value="/hortalicas/{codigoHortalica}", method=RequestMethod.GET)
    public ModelAndView detalhesHortalica(@PathVariable("codigoHortalica") long codigoHortalica) {
        Hortalica hortalica = hr.findByCodigo(codigoHortalica);
        ModelAndView mv = new ModelAndView("/hortalica/detalhesHortalica");
        mv.addObject("hortalica", hortalica);

        return mv;
    }

    //Pagina de login do administrador
    @RequestMapping("/login")
    public ModelAndView loginAdm() {
        ModelAndView mv = new ModelAndView("/adm/adm");
        return mv;
    }

    //Pagina de cadastrar itens de troca
    @RequestMapping("/cadastrarItemDeTroca")
    public ModelAndView form() {
        return new ModelAndView("itemDeTroca/formItemDeTroca.html");
    }

    //Pagina para editar itens de troca
    @RequestMapping("/itensDeTroca")
    public ModelAndView itensDeTroca() {
        return new ModelAndView("itemDeTroca/editarItemDeTroca.html");
    }
}
