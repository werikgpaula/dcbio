package DCBioHorta.controllers;


import DCBioHorta.models.ItemDeTroca;
import DCBioHorta.repository.ItemDeTrocaRepository;

import javax.validation.Valid;


import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.*;


import DCBioHorta.DcBioHortaApplication;


@RestController
public class ItemDeTrocaController {

    @Autowired
    private ItemDeTrocaRepository ir;


    @RequestMapping(value="/cadastrarItemDeTroca", method=RequestMethod.POST)
    public RedirectView Cadastrar(@Valid ItemDeTroca itemDeTroca, BindingResult result, RedirectAttributes attributes) {
    	if(result.hasErrors()) {
            attributes.addFlashAttribute("mensagem", result.getFieldError().getDefaultMessage());
            return new RedirectView("/cadastrarItemDeTroca");
        }
    	attributes.addFlashAttribute("mensagem", "Item de troca adicionado com sucesso!");
        ir.save(itemDeTroca);
        return new RedirectView("/cadastrarItemDeTroca");
    }

    @RequestMapping("/carregarItensDeTroca")
    public List<ItemDeTroca> getItensDeTroca() {
        List<ItemDeTroca> itensDeTroca = ir.findAll();
        return itensDeTroca;
    }

    @RequestMapping("/carregarItemDeTroca/{codigoItemDeTroca}")
    public ItemDeTroca getItemDeTrocaCodigo(@PathVariable("codigoItemDeTroca") long codigoItemDeTroca) {
        ItemDeTroca itemDeTroca = ir.findByCodigoItemDeTroca(codigoItemDeTroca);
        return itemDeTroca;
    }

    @RequestMapping("/carregarItensDeTroca/{nomeItemDeTroca}")
    public ItemDeTroca getItemDeTroca(@PathVariable("nomeItemDeTroca") String nomeItemDeTroca) {
        ItemDeTroca itemDeTroca = ir.findByNomeItemDeTroca(nomeItemDeTroca);
        return itemDeTroca;
    }



    @RequestMapping(value="cadastrarItemDeTroca/deletar/{codigoItemDeTroca}")
    public RedirectView deletarItemDeTroca(@PathVariable("codigoItemDeTroca") long codigoItemDeTroca){
        ItemDeTroca itemDeTroca = ir.findByCodigoItemDeTroca(codigoItemDeTroca);
        ir.delete(itemDeTroca);
        return new RedirectView("/cadastrarItemDeTroca");
    }



}


