package DCBioHorta.controllers;

import javax.validation.Valid;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.validation.BindingResult;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import DCBioHorta.DcBioHortaApplication;
import DCBioHorta.models.Evento;
import DCBioHorta.models.Hortalica;
import DCBioHorta.repository.HortalicaRepository;


@RestController
public class HortalicaController {
	@Autowired 
	private HortalicaRepository hr;

	//Função para cadastrar a hortaliça 
	@RequestMapping(value="/cadastrarHortalica", headers="content-type=multipart/*", method=RequestMethod.POST)
	public RedirectView Cadastrar(@Valid Hortalica hortalica, BindingResult result, @RequestParam("file") MultipartFile file, RedirectAttributes attributes) {
		if(result.hasErrors()) {
			attributes.addFlashAttribute("mensagem", result.getFieldError().getDefaultMessage());
			return new RedirectView("/cadastrarHortalica");
		}
		try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get("../DCBioHorta/src/main/resources/static/imagensHortalicas/" + file.getOriginalFilename());
            Files.write(path, bytes);
            hortalica.setImagem("../imagensHortalicas/" + file.getOriginalFilename());

        } catch (IOException e) {
            e.printStackTrace();
        }
		hr.save(hortalica);
		attributes.addFlashAttribute("mensagem", "Cadastro realizado com sucesso!");
		return new RedirectView("/cadastrarHortalica");
	}

	//Função para carregar a tela de cadastro de hortaliça
	@RequestMapping("/cadastrarHortalica")
	public ModelAndView CadastroHortalica() {
		ModelAndView mv = new ModelAndView("hortalica/formHortalica");
		return mv;
	}
	
	//Função que retorna uma lista de hortaliças ordenadas alfabeticamente
	@RequestMapping("/carregarhortalicas")
	public List<Hortalica> getHortalicas() {
		List<Hortalica> hortalicas = hr.findAll();
		List<Hortalica> hortalicasOrdenadas = hortalicas.stream().sorted(Comparator.comparing(Hortalica::getNome)).collect(Collectors.toList());
		
		return hortalicasOrdenadas;
	}
	@RequestMapping("/hortalicas")
	public ModelAndView hortalicas() {
		return new ModelAndView("hortalica/hortalicas");
	}
	

	//Função para retornar uma hortaliça pelo código
	@RequestMapping(value="/hortalicas/detalhesHortalica")
	public Hortalica findByCodigoHortalica(long codigoHortalica){
		return hr.findByCodigo(codigoHortalica);
	}
	
	@RequestMapping("/editarHortalicas")
	public ModelAndView edicaoHortalicas() {
		return new ModelAndView("hortalica/editarHortalica");
	}
	
	@RequestMapping(value="/editarHortalicas/editar", headers="content-type=multipart/*", method=RequestMethod.POST)
	public RedirectView Editar(@Valid Hortalica hortalica, @RequestParam("file") MultipartFile file,  BindingResult result, RedirectAttributes attributes) {
		if(result.hasErrors()) {
			attributes.addFlashAttribute("mensagem", "Verifique os campos!");
			return new RedirectView("/hortalicas/editarHortalica");
		}
		try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get("../DCBioHorta/src/main/resources/static/imagensHortalicas/" + file.getOriginalFilename());
            Files.write(path, bytes);
            hortalica.setImagem("../imagensHortalicas/" + file.getOriginalFilename());

        } catch (IOException e) {
            e.printStackTrace();
        }
		hr.save(hortalica);
		attributes.addFlashAttribute("mensagem", "Edição realizada com sucesso!");
		return new RedirectView("/hortalicas/editarHortalica");
	}
	
	//Função para deletar uma hortaliça
	@RequestMapping(value="/editarHortalicas/deletar/{codigoHortalica}", method=RequestMethod.GET)
	public RedirectView deletarHortalica(@PathVariable("codigoHortalica") long codigoHortalica) {
		Hortalica hortalica = hr.findByCodigo(codigoHortalica);
		hr.delete(hortalica);
		return new RedirectView("/editarHortalicas");
	}
}
