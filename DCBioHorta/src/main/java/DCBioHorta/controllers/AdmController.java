package DCBioHorta.controllers;

import DCBioHorta.models.Administrador;
import DCBioHorta.repository.AdministradorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AdmController {
    @Autowired
    private AdministradorRepository adm;

    @RequestMapping(value = "/admLogin")
    public Administrador findByEmailAdm(String emailAdm){return adm.findByEmailAdm(emailAdm);}

}
