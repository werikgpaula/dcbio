package DCBioHorta.controllers;

import DCBioHorta.models.Evento;
import DCBioHorta.repository.EventoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import DCBioHorta.models.Participante;
import DCBioHorta.repository.ParticipanteRepository;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
public class ParticipantesController {
	@Autowired
	private EventoRepository er;

	@Autowired
	private ParticipanteRepository pr;
	
	@RequestMapping(value="/cadastrarParticipante", method=RequestMethod.GET)
	public String form() {
		return "evento/formParticipante";	}
	
	@RequestMapping(value="/cadastrarParticipante", method=RequestMethod.POST)
	public String form(Participante participante) {
		
		pr.save(participante);
		return "redirect:/cadastrarParticipante";
	}
	//Método que cadastra o participante em um evento.

	@RequestMapping(value="/{codigoEvento}", method=RequestMethod.POST)
	public String cadastrarParticipante(@PathVariable("codigoEvento") long codigoEvento, @Valid Participante participante,
										BindingResult result, RedirectAttributes attributes) {
		if(result.hasErrors()) {
			attributes.addFlashAttribute("mensagem", "Verifique os campos!");
			return "redirect:/{codigoEvento}";
		}
		Evento evento = er.findByCodigoEvento(codigoEvento);
		participante.setEvento(evento);
		pr.save(participante);
		attributes.addFlashAttribute("mensagem", "Cadastro realizado com sucesso!");
		return "redirect:/{codigoEvento}";
	}

}
