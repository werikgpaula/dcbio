package DCBioHorta.repository;

import DCBioHorta.models.Administrador;
import org.springframework.data.jpa.repository.JpaRepository;

import org.thymeleaf.standard.processor.AbstractStandardDoubleAttributeModifierTagProcessor;

public interface AdministradorRepository extends JpaRepository<Administrador, String> {
    Administrador findByEmailAdm(String emailAdm);
}
