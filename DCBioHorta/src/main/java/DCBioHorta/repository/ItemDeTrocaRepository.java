package DCBioHorta.repository;

import DCBioHorta.models.ItemDeTroca;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ItemDeTrocaRepository extends JpaRepository<ItemDeTroca, String>{
    ItemDeTroca findByCodigoItemDeTroca(long codigoItemDeTroca);
    ItemDeTroca[] findByCodigoItemDeTroca(long[] codigoItemDeTroca);
    ItemDeTroca findByNomeItemDeTroca (String nomeItemDeTroca);
}
