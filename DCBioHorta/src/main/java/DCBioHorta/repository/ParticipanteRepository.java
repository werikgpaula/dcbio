package DCBioHorta.repository;

import org.springframework.data.repository.CrudRepository;

import DCBioHorta.models.Evento;
import DCBioHorta.models.Participante;

public interface ParticipanteRepository extends CrudRepository<Participante, String>{
	Iterable<Participante> findByEvento(Evento evento);
	Participante findByCodigoParticipante(long codigoParticipante);
}
