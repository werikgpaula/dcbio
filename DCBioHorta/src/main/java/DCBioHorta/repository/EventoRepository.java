package DCBioHorta.repository;

//import org.springframework.data.repository.CrudRepository;
import DCBioHorta.models.ItemDeTroca;
import org.springframework.data.jpa.repository.JpaRepository;
import DCBioHorta.models.Evento;

import java.util.List;


public interface EventoRepository extends JpaRepository<Evento, String>{
	Evento findByCodigoEvento(long codigoEvento);
	List<Evento> findAllByOrderByDataEventoAsc();

}

