package DCBioHorta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import DCBioHorta.models.Hortalica;


public interface HortalicaRepository extends JpaRepository<Hortalica, String>{
	Hortalica findByCodigo(long codigoHortalica);
	Hortalica findByNome(String nomeHortalica);
}
